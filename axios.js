var axios = require('axios');
var fs = require('fs');
/*var template = require('./template_json');*/

/*var url = 'http://minotaria.net/base/_pruebas/db_export.php';
var params = {
    params: {
        ver: 'todo',
        exp: '17/7/1',
        code: 'si'
    }
}*/

function getExp(expediente) {
    var url = `http://minotaria.net/base/_pruebas/db_export.php?ver=todo&exp=${expediente}&decode=si`

    return axios.get(url)
        .then(res => {
            return res.data[0]
        })
        .catch(err => console.log(`${err}`));
}

getExp('17/7/1')
    .then(data => console.log(data))
    .catch(err => console.log(`${err}`));