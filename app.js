var express = require('express');
var app = express();
var docxtemplater = require('docxtemplater');
var path = require('path');
var bodyParser = require('body-parser')
var upload = require('./modules/upload');
var conversion = require('./modules/conversion');
var tags = require('./modules/tags');

var port = process.env.PORT || 8080;

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/views'));
app.use(bodyParser.urlencoded({
    extended: true
}));

app.get('/', function(req, res) {
	res.render('index');
});

app.get('/upload', function(req, res) {
	res.render('upload');
});

app.post('/upload', function(req, res) {
	upload.searchDocs(req, res, function(err, rutaDocx, rutaJson) {

		if (err == false) {
			res.render('index');
		} else {
			var tagsDocx = tags.tags(rutaDocx);
			tags.tagsMissing(rutaJson, function(content) {
	            res.render("form", {
	                propiedad: content,
	                rutaDocx: rutaDocx,
					rutaJson: rutaJson
	            });
			});
		}
		/*if (err == false) {
			res.render('index');
		} else {
			conversion.docxtemplater(rutaDocx, rutaJson, function(file) {
				res.download(file);
			});
		}*/
	});
});

app.post('/cargar', function(req, res) {

	var tagsNews = req.body;
	var rutaJson = req.body.rutaJson;
	var rutaDocx = req.body.rutaDocx;
	var json = tags.leerJson(rutaJson, tagsNews);

	conversion.docxtemplater(rutaDocx, json, function(file) {
		res.download(file);
	});

	//res.send(json);
});

app.listen(port)
