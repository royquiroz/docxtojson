var fs = require('fs');
var path = require('path');
var Docxtemplater = require('docxtemplater');
var JSZip = require('jszip');
var expressions = require('angular-expressions');
var parse = require('./functions/util');

exports.tags = function(ruta) {

	expressions.filters.lower = function(input) {
	    if (!input) return input;
	    return input.toLowerCase();
	}

	var angularParser = function(tag) {
	    return {
	        get: tag === '.' ? function(s) {
	            return s;
	        } : expressions.compile(tag)
	    };
	}

    var content = fs.readFileSync(ruta, "binary");

    var zip = new JSZip(content);
    var doc = new Docxtemplater().loadZip(zip);

    doc.setOptions({
        parser: angularParser
    });

    doc.setData({
      //Datos para sustituir por los tags
    });

    doc.render();

    var buf = doc.getZip().generate({
        type: "nodebuffer"
    });

    var rutaJson = path.join(__dirname, '../tmp/tags.json');
    var json = fs.readFileSync(rutaJson, 'binary')

    return json;
}


exports.tagsMissing = function(rutaJson, callback) {

	var tagsDocx = fs.readFileSync('./tmp/tags.json', 'binary');
	var json = fs.readFileSync(rutaJson, 'binary');

	tagsDocx = parse.parseJson(tagsDocx);
	json = parse.parseJson(json);

	for (var x = 0; x < tagsDocx.length; x++) {

		for (var y = 0; y < json.length; y++) {

			if (tagsDocx[x] === json[y]) {
				parse.removeItemArr(tagsDocx, tagsDocx[x]);
			}

		}
	}
	callback(tagsDocx);
}


exports.leerJson = function(rutaJson, tagsNews) {
	var json = fs.readFileSync(rutaJson, 'binary');
	json = JSON.parse(json);

	delete tagsNews.rutaDocx;
	delete tagsNews.rutaJson;

	json = Object.assign(json, tagsNews);

	return json;

}
