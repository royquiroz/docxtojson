exports.parseJson = function(json) {
	var jsonParse = JSON.parse(json);
	var arr = [];

	for(var i in jsonParse) {
	    if (jsonParse.hasOwnProperty(i)) {
	        arr.push(i);
	    }
	}
	return arr;
}

exports.removeItemArr = function(arr, item) {
	var i = arr.indexOf(item);

	if(i != -1) {
		arr.splice(i, 1)
	}
}
