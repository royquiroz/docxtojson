var fs = require('fs');
var path = require('path');
var Docxtemplater = require('docxtemplater');
var JSZip = require('jszip');
var expressions = require('angular-expressions');
var fileSys = require('./functions/fs');
var parse = require('./functions/util');

expressions.filters.lower = function(input) {
    if (!input) return input;
    return input.toLowerCase();
}

var angularParser = function(tag) {
    return {
        get: tag === '.' ? function(s) {
            return s;
        } : expressions.compile(tag)
    };
}

exports.docxtemplater = function(rutaDocx, json, callback) {

	fileSys.deleteDocAnt();

	var time = Date.now();
	var valor = Math.floor(Math.random() * (time));
	var nameNewDocx = valor + '.docx';

    var download = path.join(__dirname, '../download/' + nameNewDocx);
    var contentDocx = fs.readFileSync(rutaDocx, 'binary');
	//var contentJson = fs.readFileSync(rutaJson, 'binary');

    var zip = new JSZip(contentDocx);
    var doc = new Docxtemplater().loadZip(zip);

    doc.setOptions({
        parser: angularParser
    });

	//contentJson = parse.parseJson(contentJson);
    doc.setData(json);

    doc.render();

    var buf = doc.getZip().generate({
        type: "nodebuffer"
    });

    fs.writeFileSync(download, buf);

    console.log('Listo!');
    callback(download);
}
