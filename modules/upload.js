var multiparty = require('multiparty');
var http = require('http');
var fs = require('fs');

var validation = require('./functions/files')

exports.searchDocs = function(req, res, callback) {
	if(req.url === '/upload' && req.method === 'POST') {
		var form = new multiparty.Form();

		form.parse(req, function(err, fields, files) {
			var docx = validation.validateDocx(files);
			var json = validation.validateJson(files);

			if (!docx) {
				callback(false, null, null, null);
			} else if (!json) {
				callback(false, null, null, null);
			} else {
				var rutaDocx = files.docx[0].path;
				var rutaJson = files.json[0].path;
				//var name  = files.docx[0].originalFilename;

				callback(true, rutaDocx, rutaJson);
			}
		});
	}
}
